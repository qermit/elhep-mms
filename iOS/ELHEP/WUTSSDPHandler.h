//
//  WUTSSDPHandler.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 6/19/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^SSDPIPAddressFoundBlock)(NSString *ipAddress);

@class GCDAsyncUdpSocket;

@interface WUTSSDPHandler : NSObject{
    GCDAsyncUdpSocket *udpSearchSocket_;
    GCDAsyncUdpSocket *udpConnectSocket_;
}

@property (nonatomic, copy) SSDPIPAddressFoundBlock didFoundMMSDeviceBlock;

+ (WUTSSDPHandler *)sharedHandler;

- (BOOL)startSendingAdvertisements;

- (BOOL)startListeningForAnswers;

- (BOOL)stopSendingAdvertisements;

- (BOOL)stopListeningForAnswers;

@end
