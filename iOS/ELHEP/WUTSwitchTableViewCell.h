//
//  WUTSwitchTableViewCell.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 7/18/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WUTSwitchTableViewCell : UITableViewCell

@property (nonatomic, strong) UISwitch *slider;

- (void)addTargetToSwitch:(id)target action:(SEL)selector;

@end
