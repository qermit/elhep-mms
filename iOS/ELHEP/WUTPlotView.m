//
//  WUTPlotView.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/30/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "WUTPlotView.h"
#import "WUTResult.h"


@interface WUTPlotView (){
    struct {
        unsigned int dataSourceNumberOfSamples:1;
        unsigned int dataSourceSampleForIndex:1;
    } _dataSourceFlags;
}


@end

@implementation WUTPlotView

- (id)initWithFrame:(CGRect)frame dataSource:(id<WUTPlotViewDataSource>)dataSource
{
    self = [super initWithFrame:frame];
    if (self) {
        self.dataSource = dataSource;
        self.backgroundColor = [UIColor whiteColor];
        self.boundsColor = [UIColor blackColor];
        self.textColor = [UIColor blueColor];
        self.lineColor = [UIColor redColor];
        self.minValue = 0.0f;
        self.unitsMarginWidth = 30.0f;
        self.maxValue = 1.0f;
        self.opaque = YES;
        
    }
    return self;
}

- (void)setDataSource:(id<WUTPlotViewDataSource>)dataSource{
    
    if (_dataSource != dataSource) {
        _dataSourceFlags.dataSourceSampleForIndex = [dataSource respondsToSelector:@selector(plotView:sampleAtIndex:)];
        _dataSourceFlags.dataSourceNumberOfSamples = [dataSource respondsToSelector:@selector(numberOfSamplesForPlotView:)];
        
        _dataSource = dataSource;
    }   
}

- (void)didMoveToSuperview{
    [self setNeedsDisplay];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGRect plotRect = CGRectMake(_unitsMarginWidth, 0.0f, CGRectGetWidth(rect)-_unitsMarginWidth, CGRectGetHeight(rect));
    
    
    // Draw background      
    CGContextSetFillColorWithColor(ctx, self.backgroundColor.CGColor);
    CGContextFillRect(ctx, plotRect);

    
    // Draw bounds
    CGContextSetStrokeColorWithColor(ctx, self.boundsColor.CGColor);
    CGContextSetLineWidth(ctx, 3.0f);
    CGContextStrokeRect(ctx, plotRect);
    
    // Invert CTM to use 'norma' coordinates
    CGContextSaveGState(ctx);
    CGContextTranslateCTM(ctx, 0.0f, CGRectGetMaxY(rect));
    CGContextScaleCTM(ctx, 1.0f, -1.0f);
    
    NSInteger count;
    
    // If delegate responds to selector, get how many there are ready to draw
    if (_dataSourceFlags.dataSourceNumberOfSamples){
        count = [self.dataSource numberOfSamplesForPlotView:self];
    }
    else {
        CGContextRestoreGState(ctx);
        return;
    }
    
    NSInteger width = (NSInteger)CGRectGetWidth(rect) - _unitsMarginWidth;
    CGFloat height = CGRectGetHeight(rect);
    {
        
        CGContextSetStrokeColorWithColor(ctx, self.lineColor.CGColor);
        
        CGMutablePathRef path = CGPathCreateMutable();
        CGPathMoveToPoint(path, NULL, 0, 0);
        
        
        // If there is not enough samples to draw whole width, draw all we can
        if (count < width) {
            
            for (int i = 0; i < count; i++) {
                WUTResult *result = [self.dataSource plotView:self sampleAtIndex:count - 1 - i];
                CGPathAddLineToPoint(path, NULL, i, result.value * height);
            }
        }
        else {

            for (int i = 0; i < width; i++) {
                WUTResult *result = [self.dataSource plotView:self sampleAtIndex:count - 1 - i];
                CGPathAddLineToPoint(path, NULL, i, result.value * height);
            }
            
            
        }
        // Add path to context, set width and stoke it
        CGContextAddPath(ctx, path);
        CGContextSetLineWidth(ctx, 1.0f);
        CGContextStrokePath(ctx);
        CGPathRelease(path);
    }
    CGContextRestoreGState(ctx);
    
    // Restore default CTM at the end
    
}


@end
