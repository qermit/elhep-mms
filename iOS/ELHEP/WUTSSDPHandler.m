//
//  WUTSSDPHandler.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 6/19/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "WUTSSDPHandler.h"
#import "GCDAsyncUdpSocket.h"
#import "DDLog.h"
#import "DDTTYLogger.h"
#import "NSString+NSData.h"
#import <ifaddrs.h>
#import <arpa/inet.h>
#import "../../Common/mms_configuration.h"

// Log levels: off, error, warn, info, verbose
static const int ddLogLevel = LOG_LEVEL_VERBOSE;

@implementation WUTSSDPHandler

#pragma mark - NSObject

+ (WUTSSDPHandler *)sharedHandler{
	static WUTSSDPHandler	*this	= nil;
    
	if (!this)
		this = [[WUTSSDPHandler alloc] init];
    
	return this;
}

- (id)init{
    self = [super init];
    if(!self)return nil;
    
    dispatch_queue_t delegate_queue = dispatch_queue_create("com.mapeddELHEP.SSDPQueue", NULL);
    dispatch_queue_t socket_queue = dispatch_queue_create("com.mapeddELHEP.SocketQueue", NULL);
    
    udpSearchSocket_ = [[GCDAsyncUdpSocket alloc] initWithDelegate:self
                                               delegateQueue:delegate_queue
                                                 socketQueue:socket_queue];
    
    udpConnectSocket_ = [[GCDAsyncUdpSocket alloc] initWithDelegate:self
                                                     delegateQueue:delegate_queue
                                                       socketQueue:socket_queue];
    
    NSError *error = nil;
	
	if (![udpSearchSocket_ bindToPort:SSDP_PORT_NUMBER error:&error])
	{
		DDLogError(@"Error binding: %@", error);
		return nil;
	}
    
    
    [udpSearchSocket_ enableBroadcast:YES error:nil];
    
    return self;
}

#pragma mark - Public

- (BOOL)stopSendingAdvertisements{
    return YES;
}

- (BOOL)stopListeningForAnswers{
    return YES;
}

- (BOOL)startSendingAdvertisements{
    
    uint16_t ssdpPort                   = (uint16_t)SSDP_PORT_NUMBER;
    NSString *multicastAddress          = @(SSDP_MULTICAST_IP_ADDRESS);
    NSString *serviceName               = @(MMS_SSDP_SERVICE_NAME);
    NSString *mmsInterfaceVersionString = @(MMS_INTERFACE_VERSION);
    
    
    NSString *stringData = [NSString stringWithFormat:
                            @"M-SEARCH * HTTP/1.1\r\n"
                            @"HOST:%@:%d\r\n"
                            @"MAN: \"ssdp:discover\"\r\n"
                            @"MX: 1\r\n"
                            @"ST:urn:%@:%@\r\n\r\n"
                            ,multicastAddress,ssdpPort,serviceName, mmsInterfaceVersionString];
    
    
    
    [udpSearchSocket_ sendData:[stringData dataUsingEncoding:NSUTF8StringEncoding]
                        toHost:multicastAddress
                          port:ssdpPort
                   withTimeout:10
                           tag:12];
    
    return YES;
    
}

- (BOOL)startListeningForAnswers{
    NSError *error = nil;
    
    NSString *multicastAddress = @(SSDP_MULTICAST_IP_ADDRESS);
    
    if ([udpSearchSocket_ joinMulticastGroup:multicastAddress error:&error]) {
        DDLogError(@"join YES");
    }
    else {
        DDLogError(@"join NO: error: %@", [error localizedDescription]);
        return NO;
    }
    
    if (![udpSearchSocket_ beginReceiving:&error])
    {
        [udpSearchSocket_ close];
        
        DDLogError(@"Error starting server (recv): %@", error);
        return NO;
    }
    
    return YES;
}

#pragma mark - Private

- (NSString *)getIPAddress {
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
    
}

#pragma mark - GCDAsyncUdpSocketDelegate

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didConnectToAddress:(NSData *)address{
    DDLogInfo(@"did connect to address: %@", [NSString stringFromData:address]);
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotConnect:(NSError *)error{
    DDLogError(@"did not connect: %@", [error localizedDescription]);
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag{
    DDLogInfo(@"did send data with tag : %ld", tag);
    
    double delayInSeconds = SSDP_MULTICAST_BREAK_TIME;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self startSendingAdvertisements];
    });
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error{
    DDLogError(@"did not send data with tag : %ld", tag);   
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext{
        
    NSString *hostAddress = [GCDAsyncUdpSocket hostFromAddress:address];
    
    /* Check if the data are comming from ther IP number than ours */
    if ([hostAddress isEqualToString: [self getIPAddress]]) {
    }
    else{
        DDLogWarn(@"hostAddress: %@",hostAddress);
        
        if (self.didFoundMMSDeviceBlock) {
            self.didFoundMMSDeviceBlock(hostAddress);
        }        
    }
}

- (void)udpSocketDidClose:(GCDAsyncUdpSocket *)sock withError:(NSError *)error{
    DDLogError(@"socket did close with error: %@", [error localizedDescription]);
}


@end
