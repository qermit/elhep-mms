//
//  Measurement.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/21/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import "Measurement.h"

@implementation Measurement

- (id)initWithDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if(!self)return nil;
    
    if (![dictionary isKindOfClass:[NSDictionary class]]) {
        NSLog(@"not a dict!");
        return nil;
    }
    
    NSArray *samples = dictionary[@"probeValue"];
    
    _sample = [samples[0] floatValue];
    
    _timestamp = [dictionary[@"timestamp"] doubleValue];
    
    return self;
}

+ (Measurement *)measurementWithDict:(NSDictionary *)dict{
    return [[Measurement alloc] initWithDictionary:dict];
}

@end
