//
//  WUTTextFieldCell.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 7/18/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "WUTTextFieldCell.h"

#define TEXT_FIELD_FRAME CGRectMake(158, 5, 160, 40)

@interface WUTTextFieldCell  ()

@property (nonatomic, strong) UILabel *unitLabel;

@end

@implementation WUTTextFieldCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    CGRect textFieldFrame = TEXT_FIELD_FRAME;
    
    self.textField = [[UITextField alloc] initWithFrame:textFieldFrame];
    _textField.borderStyle = UITextBorderStyleRoundedRect;
    _textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    _textField.returnKeyType = UIReturnKeyDone;
    _textField.font = [UIFont systemFontOfSize:24.0f];
    _textField.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin;
    
    _unit = WUTUnitNone;
    
    self.unitLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 30, 40)];
    _unitLabel.backgroundColor = [UIColor clearColor];
    
    _textField.rightView = _unitLabel;
    _textField.rightViewMode = UITextFieldViewModeAlways;
    
    [self.contentView addSubview:_textField];
    
    return self;
}

- (CGRect)rightViewRectForBounds:(CGRect)bounds{
    return CGRectMake(bounds.size.width - 30.0f, 0.0, 30.0f, 40.0f);
}

- (void)setUnit:(WUTUnit)unit{
    switch (unit) {
        case WUTUnitHertz:
        {
            _unitLabel.text = @"Hz";
        }
            break;
        case WUTUnitKiloHertz:
        {
            _unitLabel.text = @"kHz";
        }
            break;
        case WUTUnitMegaHertz:
        {
            _unitLabel.text = @"MHz";
        }
            break;
        case WUTUnitGigaHertz:
        {
            _unitLabel.text = @"GHz";
        }
            break;
            
        default:
        {
            _unitLabel.text = @"";
        }
            break;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self.contentView bringSubviewToFront:_textField];
}

@end
