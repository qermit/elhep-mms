//
//  WUTTextFieldCell.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 7/18/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
	WUTUnitNone         = 0,
    
    WUTUnitMicroVolts   = 1,
    WUTUnitMiliVolts    = 2,
    WUTUnitVolts        = 3,
    WUTUnitKiloVolts    = 4,
    WUTUnitMegaVolts    = 5,
    
    WUTUnitMicroAmpers  = 6,
    WUTUnitMiliAmpers   = 7,
    WUTUnitAmpers       = 8,
    WUTUnitKiloAmpers   = 9,
    WUTUnitMegaAmperes  = 10,
    
    WUTUnitHertz        = 11,
    WUTUnitKiloHertz    = 12,
    WUTUnitMegaHertz    = 13,
    WUTUnitGigaHertz    = 14,
    
} WUTUnit;

@interface WUTTextFieldCell : UITableViewCell

@property (nonatomic, strong) UITextField *textField;

@property (nonatomic, assign) WUTUnit unit;

@end
