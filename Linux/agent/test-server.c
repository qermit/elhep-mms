/*
 * libwebsockets-test-server - libwebsockets test implementation
 *
 * Copyright (C) 2010-2011 Andy Green <andy@warmcat.com>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation:
 *  version 2.1 of the License.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *  MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include <sys/time.h>

#include "../lib/libwebsockets.h"
#include <json/json.h>


static int close_testing;

/*
 * This demo server shows how to use libwebsockets for one or more
 * websocket protocols in the same server
 *
 * It defines the following websocket protocols:
 *
 *  dumb-increment-protocol:  once the socket is opened, an incrementing
 *				ascii string is sent down it every 50ms.
 *				If you send "reset\n" on the websocket, then
 *				the incrementing number is reset to 0.
 *
 *  lws-mirror-protocol: copies any received packet to every connection also
 *				using this protocol, including the sender
 */

enum demo_protocols {
	/* always first */
	PROTOCOL_HTTP = 0,

	PROTOCOL_DUMB_INCREMENT,
	PROTOCOL_EHLEP_TEST,

	/* always last */
	DEMO_PROTOCOL_COUNT
};

json_object * build_json_schema() {
  json_object * jobj = json_object_new_object();

  json_object_object_add(jobj, "deviceID", json_object_new_int(10));
  json_object_object_add(jobj, "verID", json_object_new_double(0.1));
  json_object_object_add(jobj, "deviceName", json_object_new_string("Armputer ProbeScope"));

  json_object *probes = json_object_new_array();
  json_object *probe = json_object_new_object();
  json_object_object_add(probe, "probeID", json_object_new_int(10));
  json_object_object_add(probe, "probeName", json_object_new_string("Channel A"));

  json_object *probe_spec = json_object_new_object();
  json_object *probe_out = json_object_new_object();
  json_object *probe_in = json_object_new_object();
  json_object *probe_actions = json_object_new_array();
  json_object_object_add(probe_spec, "probeOut", probe_out);
  json_object_object_add(probe_spec, "probeIn", probe_in);
  json_object_object_add(probe_spec, "probeActions", probe_actions);

  json_object_object_add(probe_in, "triggerLow", json_object_new_int(0));
  json_object_object_add(probe_in, "triggerHigh", json_object_new_int(1));
  json_object_object_add(probe_in, "sampleFreg", json_object_new_int(10));
  json_object_object_add(probe_in, "autoTriggerRetry", json_object_new_boolean(TRUE));
  json_object_object_add(probe_in, "numberOfSamples", json_object_new_int(1));

  json_object_object_add(probe_out, "probeType", json_object_new_string("A"));
  json_object_object_add(probe_out, "dataType", json_object_new_string("d1"));
  json_object_object_add(probe_out, "dataUnit", json_object_new_string("mA"));
  json_object_object_add(probe_out, "defaultDisplayType", json_object_new_string("label"));
  json_object_object_add(probe, "probeSpec", probe_spec);

  json_object_array_add(probes, probe);
  json_object_object_add(jobj, "uniqueProbes", probes);
//printf ("The json object created:\n%s\n",json_object_to_json_string(jobj));

  return jobj;
}

json_object * build_json_ack(int status) {
  json_object * jobj = json_object_new_object();

  json_object_object_add(jobj, "status", json_object_new_int(status));
  if (status != 0) {
    json_object_object_add(jobj, "error", json_object_new_string("Fatal error"));
  }

  return jobj;
}

int licznik = 0;

json_object * build_json_fakedata() {
  json_object * jobj = json_object_new_object();
  json_object_object_add(jobj, "deviceID", json_object_new_int(10));

  json_object *measurements = json_object_new_array();
  json_object *meas = json_object_new_object();
  json_object_object_add(meas, "probeID", json_object_new_int(10));
  json_object_object_add(meas, "timestamp", json_object_new_int(licznik++));


  json_object *probe_val = json_object_new_array();
  json_object_array_add(probe_val, json_object_new_int(licznik+10));
  json_object_array_add(probe_val, json_object_new_int(licznik-10));
  json_object_object_add(meas, "probeValue", probe_val);

  json_object_array_add( measurements, meas);
  json_object_object_add(jobj, "measurements", measurements);

  return jobj;
}


//#define LOCAL_RESOURCE_PATH INSTALL_DATADIR"/libwebsockets-test-server"
#define LOCAL_RESOURCE_PATH "./www-data"
/* this protocol server (always the first one) just knows how to do HTTP */

static int callback_http(struct libwebsocket_context *context,
		struct libwebsocket *wsi,
		enum libwebsocket_callback_reasons reason, void *user,
							   void *in, size_t len)
{
	char client_name[128];
	char client_ip[128];


	char servefile[128];
	servefile[0] = 0;

	switch (reason) {
	case LWS_CALLBACK_HTTP:
		fprintf(stderr, "serving HTTP URI %s\n", (char *)in);

        char* pos = strchr(in, '?');
        if (pos != NULL) {
            *pos = 0;
        }
        strncat(servefile, LOCAL_RESOURCE_PATH, 127);
        if ( strcmp(in, "/") == 0) {
            strncat(servefile, "/index.html", 127);
        } else {
            strncat(servefile, in, 127);
        }

        if (libwebsockets_serve_http_file(wsi,
                  servefile, "text/html"))
            fprintf(stderr, "Failed to send HTTP file\n");
        break;

	/*
	 * callback for confirming to continue with client IP appear in
	 * protocol 0 callback since no websocket protocol has been agreed
	 * yet.  You can just ignore this if you won't filter on client IP
	 * since the default uhandled callback return is 0 meaning let the
	 * connection continue.
	 */

	case LWS_CALLBACK_FILTER_NETWORK_CONNECTION:

		libwebsockets_get_peer_addresses((int)(long)user, client_name,
			     sizeof(client_name), client_ip, sizeof(client_ip));

		fprintf(stderr, "Received network connect from %s (%s)\n",
							client_name, client_ip);

		/* if we returned non-zero from here, we kill the connection */
		break;

	default:
		break;
	}

	return 0;
}


/*
 * this is just an example of parsing handshake headers, you don't need this
 * in your code unless you will filter allowing connections by the header
 * content
 */

static void
dump_handshake_info(struct lws_tokens *lwst)
{
	int n;
	static const char *token_names[WSI_TOKEN_COUNT] = {
		/*[WSI_TOKEN_GET_URI]		=*/ "GET URI",
		/*[WSI_TOKEN_HOST]		=*/ "Host",
		/*[WSI_TOKEN_CONNECTION]	=*/ "Connection",
		/*[WSI_TOKEN_KEY1]		=*/ "key 1",
		/*[WSI_TOKEN_KEY2]		=*/ "key 2",
		/*[WSI_TOKEN_PROTOCOL]		=*/ "Protocol",
		/*[WSI_TOKEN_UPGRADE]		=*/ "Upgrade",
		/*[WSI_TOKEN_ORIGIN]		=*/ "Origin",
		/*[WSI_TOKEN_DRAFT]		=*/ "Draft",
		/*[WSI_TOKEN_CHALLENGE]		=*/ "Challenge",

		/* new for 04 */
		/*[WSI_TOKEN_KEY]		=*/ "Key",
		/*[WSI_TOKEN_VERSION]		=*/ "Version",
		/*[WSI_TOKEN_SWORIGIN]		=*/ "Sworigin",

		/* new for 05 */
		/*[WSI_TOKEN_EXTENSIONS]	=*/ "Extensions",

		/* client receives these */
		/*[WSI_TOKEN_ACCEPT]		=*/ "Accept",
		/*[WSI_TOKEN_NONCE]		=*/ "Nonce",
		/*[WSI_TOKEN_HTTP]		=*/ "Http",
		/*[WSI_TOKEN_MUXURL]	=*/ "MuxURL",
	};

	for (n = 0; n < WSI_TOKEN_COUNT; n++) {
		if (lwst[n].token == NULL)
			continue;

		fprintf(stderr, "    %s = %s\n", token_names[n], lwst[n].token);
	}
}

/* dumb_increment protocol */

/*
 * one of these is auto-created for each connection and a pointer to the
 * appropriate instance is passed to the callback in the user parameter
 *
 * for this example protocol we use it to individualize the count for each
 * connection.
 */

struct per_session_data__dumb_increment {
	int number;
};

static int
callback_dumb_increment(struct libwebsocket_context *context,
			struct libwebsocket *wsi,
			enum libwebsocket_callback_reasons reason,
					       void *user, void *in, size_t len)
{
	int n;
	unsigned char buf[LWS_SEND_BUFFER_PRE_PADDING + 512 +
						  LWS_SEND_BUFFER_POST_PADDING];
	unsigned char *p = &buf[LWS_SEND_BUFFER_PRE_PADDING];
	struct per_session_data__dumb_increment *pss = user;

	switch (reason) {

	case LWS_CALLBACK_ESTABLISHED:
		fprintf(stderr, "callback_dumb_increment: "
						 "LWS_CALLBACK_ESTABLISHED\n");
		pss->number = 0;
		break;

	/*
	 * in this protocol, we just use the broadcast action as the chance to
	 * send our own connection-specific data and ignore the broadcast info
	 * that is available in the 'in' parameter
	 */

	case LWS_CALLBACK_BROADCAST:
		n = sprintf((char *)p, "%d", pss->number++);
		n = libwebsocket_write(wsi, p, n, LWS_WRITE_TEXT);
		if (n < 0) {
			fprintf(stderr, "ERROR writing to socket");
			return 1;
		}
		if (close_testing && pss->number == 50) {
			fprintf(stderr, "close tesing limit, closing\n");
			libwebsocket_close_and_free_session(context, wsi,
						       LWS_CLOSE_STATUS_NORMAL);
		}
		break;

	case LWS_CALLBACK_RECEIVE:
		fprintf(stderr, "rx %d\n", (int)len);
		if (len < 6)
			break;
		if (strcmp(in, "reset\n") == 0)
			pss->number = 0;
		break;
	/*
	 * this just demonstrates how to use the protocol filter. If you won't
	 * study and reject connections based on header content, you don't need
	 * to handle this callback
	 */

	case LWS_CALLBACK_FILTER_PROTOCOL_CONNECTION:
		dump_handshake_info((struct lws_tokens *)(long)user);
		/* you could return non-zero here and kill the connection */
		break;

	default:
		break;
	}

	return 0;
}


/* lws-mirror_protocol */

#define MAX_MESSAGE_QUEUE 64

struct per_session_data__elhep_test {
	struct libwebsocket *wsi;
	int ringbuffer_tail;
	int state;
};

json_object * elhep_test_schema;

static int
callback_elhep_test(struct libwebsocket_context *context,
			struct libwebsocket *wsi,
			enum libwebsocket_callback_reasons reason,
					       void *user, void *in, size_t len)
{
	int n;
	struct per_session_data__elhep_test *pss = user;

	unsigned char buf[LWS_SEND_BUFFER_PRE_PADDING + 512 +
						  LWS_SEND_BUFFER_POST_PADDING];
	unsigned char *p = &buf[LWS_SEND_BUFFER_PRE_PADDING];

	switch (reason) {

	case LWS_CALLBACK_ESTABLISHED:
		fprintf(stderr, "callback_elhep_test: "
						 "LWS_CALLBACK_ESTABLISHED\n");
		pss->state = 0;libwebsocket_callback_on_writable(context, wsi);
		break;

	case LWS_CALLBACK_SERVER_WRITEABLE:
		fprintf(stderr, "cet[LWS_CALLBACK_SERVER_WRITEABLE]:%d\n", pss->state);
		if (pss->state == 0) {
			n = snprintf((char *)p, 512, "%s",json_object_to_json_string(elhep_test_schema));
			n = libwebsocket_write(wsi, p, n, LWS_WRITE_TEXT);
			pss->state =1;
			fprintf(stderr, "cet[libwebsocket_write]:%d\n", pss->state);
		} else if (pss->state == 2) {
			n = snprintf((char *)p, 512, "%s",json_object_to_json_string(build_json_ack(0)));
			n = libwebsocket_write(wsi, p, n, LWS_WRITE_TEXT);
			pss->state =3;
			fprintf(stderr, "cet[libwebsocket_write]:%d\n", pss->state);
			libwebsocket_callback_on_writable(context, wsi);
//			libwebsocket_callback_on_writable(context, wsi);
		}
		break;
	case LWS_CALLBACK_BROADCAST:
		if (pss->state == 3 ) {
			n = snprintf((char *)p, 512, "%s",json_object_to_json_string(build_json_fakedata()));
			n = libwebsocket_write(wsi, p, n, LWS_WRITE_TEXT);
			fprintf(stderr, "cet[libwebsocket meas]:%d\n", pss->state);
			libwebsocket_callback_on_writable(context, wsi);
		}
		break;

	case LWS_CALLBACK_RECEIVE:
		fprintf(stderr, "cet[LWS_CALLBACK_RECIEVE:%d\n", pss->state);
		if (pss->state == 0) break;
		/* Tutaj parsowac */
		struct json_object* new_obj = json_tokener_parse(in);
		fprintf(stderr, "data:\n%s", json_object_to_json_string(new_obj));
		pss->state = 2;
		libwebsocket_callback_on_writable(context, wsi);


		break;
	/*
	 * this just demonstrates how to use the protocol filter. If you won't
	 * study and reject connections based on header content, you don't need
	 * to handle this callback
	 */

	case LWS_CALLBACK_FILTER_PROTOCOL_CONNECTION:
		dump_handshake_info((struct lws_tokens *)(long)user);
		/* you could return non-zero here and kill the connection */
		break;

	default:
		break;
	}

	return 0;
}


/* list of supported protocols and callbacks */

static struct libwebsocket_protocols protocols[] = {
	/* first protocol must always be HTTP handler */

	{
		"http-only",		/* name */
		callback_http,		/* callback */
		0			/* per_session_data_size */
	},
	{
		"dumb-increment-protocol",
		callback_dumb_increment,
		sizeof(struct per_session_data__dumb_increment),
	},
	{
		"elhep-test",
		callback_elhep_test,
		sizeof(struct per_session_data__elhep_test)
	},
	{
		NULL, NULL, 0		/* End of list */
	}
};

static struct option options[] = {
	{ "help",	no_argument,		NULL, 'h' },
	{ "port",	required_argument,	NULL, 'p' },
	{ "ssl",	no_argument,		NULL, 's' },
	{ "killmask",	no_argument,		NULL, 'k' },
	{ "interface",  required_argument,	NULL, 'i' },
	{ "closetest",  no_argument,		NULL, 'c' },
	{ NULL, 0, 0, 0 }
};

int main(int argc, char **argv)
{
	int n = 0;
	const char *cert_path =
			    LOCAL_RESOURCE_PATH"/libwebsockets-test-server.pem";
	const char *key_path =
			LOCAL_RESOURCE_PATH"/libwebsockets-test-server.key.pem";
	unsigned char buf[LWS_SEND_BUFFER_PRE_PADDING + 1024 +
						  LWS_SEND_BUFFER_POST_PADDING];
	int port = 7681;
	int use_ssl = 0;
	struct libwebsocket_context *context;
	int opts = 0;
	char interface_name[128] = "";
	const char *interface = NULL;


	elhep_test_schema = build_json_schema();
#ifdef LWS_NO_FORK
	unsigned int oldus = 0;
#endif

	fprintf(stderr, "libwebsockets test server\n"
			"(C) Copyright 2010-2011 Andy Green <andy@warmcat.com> "
						    "licensed under LGPL2.1\n");

	while (n >= 0) {
		n = getopt_long(argc, argv, "ci:khsp:", options, NULL);
		if (n < 0)
			continue;
		switch (n) {
		case 's':
			use_ssl = 1;
			break;
		case 'k':
			opts = LWS_SERVER_OPTION_DEFEAT_CLIENT_MASK;
			break;
		case 'p':
			port = atoi(optarg);
			break;
		case 'i':
			strncpy(interface_name, optarg, sizeof interface_name);
			interface_name[(sizeof interface_name) - 1] = '\0';
			interface = interface_name;
			break;
		case 'c':
			close_testing = 1;
			fprintf(stderr, " Close testing mode -- closes on "
					   "client after 50 dumb increments"
					   "and suppresses lws_mirror spam\n");
			break;
		case 'h':
			fprintf(stderr, "Usage: test-server "
					     "[--port=<p>] [--ssl]\n");
			exit(1);
		}
	}

	if (!use_ssl)
		cert_path = key_path = NULL;

	context = libwebsocket_create_context(port, interface, protocols,
				libwebsocket_internal_extensions,
				cert_path, key_path, -1, -1, opts);
	if (context == NULL) {
		fprintf(stderr, "libwebsocket init failed\n");
		return -1;
	}

	buf[LWS_SEND_BUFFER_PRE_PADDING] = 'x';

#ifdef LWS_NO_FORK

	/*
	 * This example shows how to work with no forked service loop
	 */

	fprintf(stderr, " Using no-fork service loop\n");

	n = 0;
	while (n >= 0) {
		struct timeval tv;

		gettimeofday(&tv, NULL);

		/*
		 * This broadcasts to all dumb-increment-protocol connections
		 * at 20Hz.
		 *
		 * We're just sending a character 'x', in these examples the
		 * callbacks send their own per-connection content.
		 *
		 * You have to send something with nonzero length to get the
		 * callback actions delivered.
		 *
		 * We take care of pre-and-post padding allocation.
		 */

		if (((unsigned int)tv.tv_usec - oldus) > 50000) {
			libwebsockets_broadcast(
					&protocols[PROTOCOL_DUMB_INCREMENT],
					&buf[LWS_SEND_BUFFER_PRE_PADDING], 1);
			oldus = tv.tv_usec;
		}

		/*
		 * This example server does not fork or create a thread for
		 * websocket service, it all runs in this single loop.  So,
		 * we have to give the websockets an opportunity to service
		 * "manually".
		 *
		 * If no socket is needing service, the call below returns
		 * immediately and quickly.  Negative return means we are
		 * in process of closing
		 */

		n = libwebsocket_service(context, 50);
	}

#else

	/*
	 * This example shows how to work with the forked websocket service loop
	 */

	fprintf(stderr, " Using forked service loop\n");

	/*
	 * This forks the websocket service action into a subprocess so we
	 * don't have to take care about it.
	 */

	n = libwebsockets_fork_service_loop(context);
	if (n < 0) {
		fprintf(stderr, "Unable to fork service loop %d\n", n);
		return 1;
	}

	while (1) {

		usleep(100000);

		/*
		 * This broadcasts to all dumb-increment-protocol connections
		 * at 20Hz.
		 *
		 * We're just sending a character 'x', in these examples the
		 * callbacks send their own per-connection content.
		 *
		 * You have to send something with nonzero length to get the
		 * callback actions delivered.
		 *
		 * We take care of pre-and-post padding allocation.
		 */

		libwebsockets_broadcast(&protocols[PROTOCOL_EHLEP_TEST],
					&buf[LWS_SEND_BUFFER_PRE_PADDING], 1);

		libwebsockets_broadcast(&protocols[PROTOCOL_DUMB_INCREMENT],
					&buf[LWS_SEND_BUFFER_PRE_PADDING], 1);
	}

#endif

	libwebsocket_context_destroy(context);

	return 0;
}
